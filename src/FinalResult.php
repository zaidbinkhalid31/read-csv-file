<?php
error_reporting(0);
class FinalResult
{
    /**
     * 
     * 1) We can create a separate file as well where we manage all the messages. It could be class base or simple array
     * but i believe this class in only for bank detail so we can manage all the messages here. why i create protected variable so that we can access these messages in the subclass as well if needed.
     * 
     * 2) In this example, this class is supposed to be a bank detail class so messages can be easily managed here.
     * 
     * 3) I still don't understand why the first line of CSV is maintaining the messages there is no need for those we can manage all the messages and the response code in our code CSV should have only records. But in this example, I just leave the CSV as it is.
     * 
     * 4) Create an array in a class no need to define it separately. For best coding practice we need to define variables in a meaningful way like array should start with (a) an object should be (o) in this way new engineer will understand the things quickly. And we can decide the notation of defining variables like camel notation or snack notation in this case I use camel notation.
     * 
     * 5) There is no need to check the typecasting of variables as mentioned this code is from a very huge application then no need for typecasting. In PHP it is auto-detected and variables work as it is they have a value. If it is necessary then we can use is_float(mixed $value): bool or is_int(mixed $value): bool functions.
     * 
     * 
     * **/

    protected $bankAccountNumberMissing   =   "Bank account number missing";
    protected $bankBranchCodeMissing      =   "Bank branch code missing";
    protected $endToEndIdMissing          =   "End to end id missing";
    protected $generalCode                =   array('success' => 200, 'notfound' => 404);
    protected $generalMessage             =   array('success' => 'File read and data fetch', 'notfound' => 'Please check the path, there is no file.');

    private $aRecords;

    public function readCsvFile(String $f): array
    {
        $this->aRecords =   [];
        if (($document = fopen($f, "r")) !== FALSE && file_exists($f) !== FALSE) {
            while (($r = fgetcsv($document)) !== FALSE) {
                if (!empty($r)) {
                    $amt = (empty($r[8])) ? 0.0 : $r[8];
                    $ban = (empty($r[6])) ? $this->bankAccountNumberMissing : $r[6];
                    $bac = (empty($r[2])) ? $this->bankBranchCodeMissing : $r[2];
                    $e2e = (empty($r[10] . $r[11])) ? $this->endToEndIdMissing : $r[10] . $r[11];
                    $this->aRecords[] = [
                        "amount" => [
                            "currency" => (!empty($r[0])) ? $r[0] : '',
                            "subunits" => intval($amt * 100)
                        ],
                        "bank_account_name" => (!empty($r[7])) ? str_replace(" ", "_", strtolower($r[7])) : '',
                        "bank_account_number" => $ban,
                        "bank_branch_code" => $bac,
                        "bank_code" => (!empty($r[0])) ? $r[0] : '',
                        "end_to_end_id" => $e2e,
                    ];
                }
            }
            fclose($document); // After success reading need to close
            $this->aRecords  =   array(
                "filename" => basename($f),
                "document" => $f,
                "code" => $this->generalCode['success'],
                "message" => $this->generalMessage['success'],
                "records" => $this->aRecords
            );
        } else {
            $this->aRecords  =    array(
                "filename" => basename($f),
                "document" => $f,
                "code" => $this->generalCode['notfound'],
                "message" => $this->generalMessage['notfound'],
                "records" => []
            );
        }
        return $this->aRecords;
    }
}

/**
 * Uncomment below to test on browser.
 * **/

// $f = new FinalResult();
// $res = $f->readCsvFile('../tests/support/data_sample.csv');
// print "<pre>";
// print_r($res);
// print "</pre>";
